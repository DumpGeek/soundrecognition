import wave
import numpy as np
import matplotlib.pyplot as plt
from os import listdir
from os.path import isfile, join

mypath = "..\\wav\\recorded_voice"
onlyfiles = [f for f in listdir(mypath) if isfile(join(mypath, f))]

for elem in onlyfiles:
    sound_wave = wave.open(join(mypath, elem), "r")

    sound_wave_para = sound_wave.getparams()
    sound_wave_framerate = sound_wave.getframerate()

    sound_frames = sound_wave.readframes(-1)
    sound_array = np.fromstring(sound_frames, "Int16")

    aframesize = int(sound_wave_framerate * 0.7)
    aframesize = 68750

    print "Max Size: ", np.size(sound_array)
    print "Max Starting Point: ", (np.size(sound_array) - aframesize)
    print "Frame Size: ", aframesize

    plt.figure(1)
    plt.title("Original Sound Wave")
    plt.plot(sound_array)
    plt.show()

    while True:
        start_pos = int(raw_input("Start point: "))
        end_pos = start_pos + aframesize
        if end_pos <= np.size(sound_array):
            break

    new_sound_array = sound_array[start_pos:end_pos]
    print "New Frame Size", np.size(new_sound_array)

    plt.figure(2)
    plt.title("New Sound Wave")
    plt.plot(new_sound_array)
    plt.show()

    raw_input("Press Enter to generate new wave file")

    output_file = elem
    outfile = wave.open(output_file, mode='wb')
    outfile.setparams(sound_wave_para)
    outfile.writeframes(new_sound_array)
    outfile.close()
