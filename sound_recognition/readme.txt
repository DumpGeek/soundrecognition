Dependency:
	python 2.7
	matplotlib 2.0.2
	numpy

Installation:
	1) Install Python 2.7
	2) Other dependency can be installed by:
		1) Press Shift + Right-click of your mouse in this folder
		2) Click open command window here / open power-shell here
		3) Type the following in the command window:
			pip install --upgrade setuptools
			python setup.py install
		4) Manual Install (if above procedure failed)
			pip install numpy
			pip install -I matplotlib==2.0.2