import numpy

Pem_Seg1 = numpy.array([3,1,4,1,5,9,2,6,5,3,5,8,9,7,9])

lpc_order = 4
auto_coeff = numpy.zeros(lpc_order)	# autocorrelation matrix initiation
for p in range(lpc_order):
    for j in range(p, numpy.size(Pem_Seg1)):
        auto_coeff[p] += Pem_Seg1[j - p] * Pem_Seg1[j] # Summation of S[n]*S[n+1]
print "Auto Coefficient:\n", auto_coeff, "\n"

auto_coeff_size = numpy.size(auto_coeff)
lpc_LHS = numpy.empty([auto_coeff_size - 1, auto_coeff_size -1])
lpc_RHS = auto_coeff[1:]
for col_index in range(auto_coeff_size -1):
	lpc_LHS_row = numpy.empty([auto_coeff_size - 1])
	for row_index in range(auto_coeff_size -1):
		lpc_LHS_row[row_index] = abs(col_index - row_index)
		#lpc_LHS_row[row_index] = auto_coeff[abs(col_index - row_index)]
	lpc_LHS[col_index] = lpc_LHS_row

	lpc_param = numpy.dot(numpy.linalg.inv(lpc_LHS), lpc_RHS)

print "LPC Parameter:\n", lpc_param, "\n"

raw_input("Complete")
