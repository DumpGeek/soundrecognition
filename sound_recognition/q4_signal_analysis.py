import matplotlib.pyplot as plt
import wave
import numpy
import os

if __name__ == "__main__":
	# Load WAV file
	WaveFilePath = "..\\wav\\templates\\x.wav"
	sound_wave = wave.open(WaveFilePath, "r")

	# Sound Wave Parameters
	sound_wave_para = sound_wave.getparams()
	sound_wave_framerate = sound_wave.getframerate()
	sound_wave_nframes = sound_wave.getnframes()

	# Sound Wave Numpy Array Formation
		# Wave File must be in mono form
	sound_frames = sound_wave.readframes(-1)
	sound_array = numpy.fromstring(sound_frames, "Int16")
	sound_array = numpy.float32(sound_array)	# Precaution Measure to avoid overflow

	# time array generation
	t = numpy.arange(numpy.size(sound_array)) * float(1.0 / sound_wave_framerate)

	# Energy of the sound
	pwr_sound_array = numpy.square(sound_array)

	# Define threshold value
	threshold_pwr = numpy.mean(pwr_sound_array)

	# Frame Size 20ms with 0.25 non-overlapping, 0.75 overlapping
	aframesize = int(sound_wave_framerate * 0.02)
	nOverlap_aframesize = int(aframesize * 0.25)
	nframes = ((sound_wave_nframes - aframesize) / nOverlap_aframesize) + 1

	avg_pwr_nframes_lst = []
	zeroxing_cnt_nframes_lst = []

	# Average Power, Zero Crossing Count of Each Frames
	for index in range(nframes):
		start_pos = index * nOverlap_aframesize
		end_pos = start_pos + aframesize

		# Average Power
		pwr_frame_array = pwr_sound_array[start_pos:end_pos]
		avg_pwr_frame_array = numpy.mean(pwr_frame_array)

		# Zero Crossing Detection
		frame_array = sound_array[start_pos:end_pos]
		zeroxing_cnt = 0
		prev_sample = frame_array[0]
		for sample in frame_array:
			if numpy.sign(prev_sample) != numpy.sign(sample):
				zeroxing_cnt += 1

		avg_pwr_nframes_lst.append(avg_pwr_frame_array)
		zeroxing_cnt_nframes_lst.append(zeroxing_cnt)

	# List to numpy array conversion
	avg_pwr_nframes_array = numpy.array(avg_pwr_nframes_lst)
	zeroxing_cnt_nframes_array = numpy.array(zeroxing_cnt_nframes_lst)

	# Start and End point detection
	start_succ_cnt = 3
	end_succ_cnt = 5
	ending_pt = 0
	IsStart = False
	IsEnd = False
	for base_index in range(numpy.size(avg_pwr_nframes_array) - end_succ_cnt):
		counter = 0
		if IsStart == False:
			# Starting Point Detection by successive frames
			for index in range(start_succ_cnt):
				if avg_pwr_nframes_array[base_index + index] > threshold_pwr and \
					zeroxing_cnt_nframes_array[base_index + index] > 0:
					counter += 1
				if counter == start_succ_cnt:
					IsStart = True
					starting_pt = base_index
					#print "Start Point Detected"
		else:
			# End Point Detection by successive frames
			for index in range(end_succ_cnt):
				if avg_pwr_nframes_array[base_index + index] < threshold_pwr:
					counter += 1
				if counter == end_succ_cnt:
					IsEnd = True
					ending_pt = base_index
					#print "End Point Detected"
		if IsEnd == True:
			break
	if ending_pt == 0:
		ending_pt = numpy.size(avg_pwr_nframes_array) - 1

	# frame index to signal position conversion
	starting_pt = starting_pt * nOverlap_aframesize
	ending_pt = ending_pt * nOverlap_aframesize + aframesize

	# Debug Use
	print "Total Frames:\t", sound_wave_nframes
	print "Starting Point (T1):\t", starting_pt
	print "End Point (T2):\t", ending_pt, "\n"

	# The following setting is hard coded for s2A.wav
	word_two = sound_array[starting_pt:ending_pt]
	# Extract voice vowel frame from extracted signal (T1 to T2)(Hard coded)
	vowel_frame_start_pos = 10000	# ~5000 < vowel in extracted signal < End
	vowel_frame_end_pos = vowel_frame_start_pos + aframesize # aframesize = 20ms frame size
	# Extracted Segment 1
	Seg1 = word_two[vowel_frame_start_pos:vowel_frame_end_pos]
	# Save Seg1 data into Seg1_datum.txt
	numpy.savetxt("..\\doc\\Seg1_datum.txt", Seg1)

	# Discret Forier Transform (DFT) of Seg1
	# X(m = 0, 1, 2 ... N/2) = FT{S(k = 0, 1, 2, ... N-1)}
	print "Calculating Engergy by DFT ...", "\n"
	N = len(Seg1)
	X_array = numpy.zeros(N/2 + 1, dtype=numpy.complex_)
	for m in range(N/2 + 1): # m: from 0 to N/2
		for k in range(N):	# Sum Term from 0 to N-1
			X_array[m] += Seg1[k] * numpy.exp(-1j * ((2 * numpy.pi * k * m) / N))

	# Frequecy array generation
	f = numpy.arange(N/2 + 1) * (sound_wave_framerate / N)

	#Pre-emphasis Segment 1
	Pem_Seg1 = []
	PreEmphConst = 0.945
	PrevSample = 0
	for elem in Seg1:
		CurrSample = elem - PrevSample * PreEmphConst
		Pem_Seg1.append(CurrSample)
		PrevSample = CurrSample
	Pem_Seg1 = numpy.array(Pem_Seg1)

	# Auto Coorelation
	lpc_order = 10
	auto_coeff = numpy.zeros(lpc_order)	# autocorrelation matrix initiation
	for p in range(lpc_order):
		for j in range(p, aframesize):
			auto_coeff[p] += Pem_Seg1[j - p] * Pem_Seg1[j] # Summation of S[n]*S[n+1]
	print "Auto Coefficient:\n", auto_coeff, "\n"

	# Calculte LPC a[] from auto-correlation matrix using Durbin's Method
	# Method by matrix
	auto_coeff_size = numpy.size(auto_coeff)
	lpc_LHS = numpy.empty([auto_coeff_size - 1, auto_coeff_size -1])
	lpc_RHS = auto_coeff[1:]
	for col_index in range(auto_coeff_size -1):
		lpc_LHS_row = numpy.empty([auto_coeff_size - 1])
		for row_index in range(auto_coeff_size -1):
			lpc_LHS_row[row_index] = abs(col_index - row_index)
			#lpc_LHS_row[row_index] = auto_coeff[abs(col_index - row_index)]
		lpc_LHS[col_index] = lpc_LHS_row

	lpc_param = numpy.dot(numpy.linalg.inv(lpc_LHS), lpc_RHS)

	print "LPC Parameter:\n", lpc_param, "\n"
	numpy.savetxt("..\\doc\\lpc_param.txt", lpc_param)

	# Sound Wave Plot
	plt.figure(1)
	plt.title("Signal Wave")
	plt.plot(t, sound_array)

	# Fourier Transform against Frequency
	plt.figure(6)
	plt.title("Fourier Transform (Energy against Frequency)")
	# Absolute complex number will return
	plt.plot(f, abs(X_array))

	# Seg1 and Pem_Seg1 Plot
	plt.figure(7)
	plt.subplot(211)
	plt.title("Seg1")
	plt.plot(Seg1)
	plt.subplot(212)
	plt.title("Pem_Seg1")
	plt.plot(Pem_Seg1)
	plt.show()

	raw_input("COMPLETE\n\nPress Enter to exit the program")
