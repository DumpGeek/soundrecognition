#---------------------------------------------
# A brief calculation of isolated 10-word
#---------------------------------------------
#   a word = 1 seconds
#   no. of words = 10
#   no. of recorded sample per word = 5 times
#   sampling freq = 16 kHz
#   sample width = 2 (16 bits)
#   each frame size = 20 ms
#   overlap ratio = 0.5
#   no. of frames = ( 1 s - 0.02 s  ) / (0.02 * 0.5) + 1 = 99 = nearly 100 frames
#   LPC_Order = 12
#       For 12-ordered LPC:
#           Each frame generate 12 cepstral Coefficient
#   No of sample = 5 * 10 * 100 frames
#   Put all frames to train a code-book size = 64, so each frame can be represented by an index ranged from 1 to 64
#   Use DP to compare an input with each templates
#   Optain the result which as the minimum distortion

import numpy
import os

def dist(mfcc_array_a, mfcc_array_b, a_t, b_t):
    dist = 0
    a_t -= 1
    b_t -= 1
    if numpy.shape(mfcc_array_a)[0] == numpy.shape(mfcc_array_b)[0]:
        for j_index in range(1, numpy.shape(mfcc_array_a)[0]):
            sq_of_a_minus_b = numpy.square(mfcc_array_a[j_index][a_t] - mfcc_array_b[j_index][b_t])
            dist += sq_of_a_minus_b
    return numpy.sqrt(dist)

def dist_matrix(mfcc_array_a, mfcc_array_b, a_t, b_t):
    dist_matrix = numpy.zeros((numpy.shape(mfcc_array_a)[0], numpy.shape(mfcc_array_b)[0]))
    for j_index in range(0, numpy.shape(mfcc_array_a)[0]):
        for i_index in range(0, numpy.shape(mfcc_array_b)[0]):
            sq_of_a_minus_b = numpy.square(mfcc_array_a[j_index][a_t] - mfcc_array_b[i_index][b_t])
            dist_matrix[j_index][i_index] = sq_of_a_minus_b
    return dist_matrix

def acc_distort_matrix(dist_matrix):
    acc_distort_matrix = numpy.zeros((numpy.shape(dist_matrix)[0], numpy.shape(dist_matrix)[1]))

    if numpy.shape(dist_matrix)[0] == numpy.shape(dist_matrix)[1]:
        q = numpy.shape(dist_matrix)[0]
    else:
        raise IOError("Input Matrix is not a square matrix")
    for m in range(0, q):
        for n in range(m, q):
            for k in range(2):
                min_nearest_cells = 0
                if k == 0:
                    #print("%d, %d" % (m, n))
                    if m == 0 and n == 0:
                        #print("0, 0")
                        min_nearest_cells = 0
                    elif m == 0 and n != 0:
                        #print("%d, %d" % (0, n-1))
                        min_nearest_cells = acc_distort_matrix[m][n-1]
                    elif m != 0 and n != 0:
                        #print("%d, %d" % (m, n-1))
                        #print("%d, %d" % (m-1, n))
                        #print("%d, %d" % (m-1, n-1))
                        nearest_cells = numpy.empty(0)
                        nearest_cells = numpy.append(nearest_cells, acc_distort_matrix[m][n-1])
                        nearest_cells = numpy.append(nearest_cells, acc_distort_matrix[m-1][n])
                        nearest_cells = numpy.append(nearest_cells, acc_distort_matrix[m-1][n-1])
                        min_nearest_cells = numpy.min(nearest_cells)
                    acc_distort_matrix[m][n] = dist_matrix[m][n] + min_nearest_cells
                elif k == 1:
                    #print("%d, %d" % (n, m))
                    if m == 0 and n == 0:
                        #print("0, 0")
                        min_nearest_cells = 0
                    elif m == 0 and n != 0:
                        #print("%d, %d" % (n-1, 0))
                        min_nearest_cells = acc_distort_matrix[n-1][m]
                    elif m != 0 and n != 0:
                        #print("%d, %d" % (n-1, m))
                        #print("%d, %d" % (n, m-1))
                        #print("%d, %d" % (n-1, m-1))
                        nearest_cells = numpy.empty(0)
                        nearest_cells = numpy.append(nearest_cells, acc_distort_matrix[n-1][m])
                        nearest_cells = numpy.append(nearest_cells, acc_distort_matrix[n][m-1])
                        nearest_cells = numpy.append(nearest_cells, acc_distort_matrix[n-1][m-1])
                        min_nearest_cells = numpy.min(nearest_cells)
                    acc_distort_matrix[n][m] = dist_matrix[n][m] + min_nearest_cells
    return acc_distort_matrix



if __name__ == "__main__":

    # Loading file
    recorded_num_lst = [1,2,5,8]
    # Hard Coded Loading "2" only, make a loop to loop all the files
    mfcc_dat_path_a = "..\\dat\\mfcc_s%dA.csv" % recorded_num_lst[1]
    mfcc_dat_path_b = "..\\dat\\mfcc_s%dB.csv" % recorded_num_lst[1]
    mfcc_array_a = numpy.genfromtxt(mfcc_dat_path_a, delimiter=" ")
    mfcc_array_b = numpy.genfromtxt(mfcc_dat_path_b, delimiter=" ")

    # distortion equation
    # distortion between sound A at t = 28 and sound b at t 32
    # dist_x28_y32 = dist(mfcc_array_a, mfcc_array_b, 28, 32)
    # print("Distortion(Mx:28, My:32): ", dist_x28_y32)
    # with open("..\\doc\\distort_Mx28_My32.txt", "w") as ans_file:
    #     ans_file.write(str(dist_x28_y32))

    array_a = numpy.array([2, 4, 6, 9, 3, 4, 5, 8, 1])
    array_b = numpy.array([3, 5, 5, 8, 4, 2, 3, 7, 2])
    #array_a = numpy.flip(array_a, 0)
    array_a = array_a[:, None]
    array_b = array_b[:, None]
    distort_matrix = dist_matrix(array_a, array_b, 0, 0)
    print(distort_matrix)
    numpy.savetxt("..\\doc\\distort_matrix.csv", distort_matrix, delimiter = ",")

    acc_distort_matrix = acc_distort_matrix(distort_matrix)
    print(acc_distort_matrix)
    numpy.savetxt("..\\doc\\acc_distort_matrix.csv", acc_distort_matrix, delimiter = ",")
