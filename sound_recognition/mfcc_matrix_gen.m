% This is a file to generate all the mfcc matrix for later calculation
% This script should be run in Octave

% To avoid any conflict with parameters history
% Clear-up Octave parameters
clear all;
close all;
clc;

% Import library by path
addpath('..\\lib\\mfcc')

% Define variables
Tw = 25;                  % analysis frame duration (ms)
Ts = 10;                  % analysis frame shift (ms)
alpha = 0.97;             % pre-emphais coefficient
M = 20;                   % number of filterbanks channels
C = 12;                   % number of cepstral coefficeients
L = 22;                   % cepstral sine lifter parameters
LF = 20;                 % lower frequency limit (Hz)
HF = 3700;                % upper frequency limit (Hz)

% Read audio file, load speech and fs, MFCC, save matrix
wave_file = '..\\wav\\s1A.wav';
[speech, fs] = audioread( wave_file );
[MFCCs, FBE, FRAMES] = mfcc( speech , fs, Tw, Ts, alpha, @hamming, [LF HF], M, C+1, L);
save('..\\dat\\mfcc_s1A.csv', 'MFCCs','-ascii');
% CC - matrix of mel frequency cepstral coefficeients (MFCCs) with features vectors as columns
% FBE - a matrix of filterbank energies with feature vectors as columns
% FRAMES - a matrix of windowed frames (One frame per column)

wave_file = '..\\wav\\s1B.wav';
[speech, fs] = audioread( wave_file );
[MFCCs, FBE, FRAMES] = mfcc( speech , fs, Tw, Ts, alpha, @hamming, [LF HF], M, C+1, L);
save('..\\dat\\mfcc_s1B.csv', 'MFCCs','-ascii');

wave_file = '..\\wav\\s2A.wav';
[speech, fs] = audioread( wave_file );
[MFCCs, FBE, FRAMES] = mfcc( speech , fs, Tw, Ts, alpha, @hamming, [LF HF], M, C+1, L);
save('..\\dat\\mfcc_s2A.csv', 'MFCCs','-ascii');

wave_file = '..\\wav\\s2B.wav';
[speech, fs] = audioread( wave_file );
[MFCCs, FBE, FRAMES] = mfcc( speech , fs, Tw, Ts, alpha, @hamming, [LF HF], M, C+1, L);
save('..\\dat\\mfcc_s2B.csv', 'MFCCs','-ascii');

wave_file = '..\\wav\\s5A.wav';
[speech, fs] = audioread( wave_file );
[MFCCs, FBE, FRAMES] = mfcc( speech , fs, Tw, Ts, alpha, @hamming, [LF HF], M, C+1, L);
save('..\\dat\\mfcc_s5A.csv', 'MFCCs','-ascii');

wave_file = '..\\wav\\s5B.wav';
[speech, fs] = audioread( wave_file );
[MFCCs, FBE, FRAMES] = mfcc( speech , fs, Tw, Ts, alpha, @hamming, [LF HF], M, C+1, L);
save('..\\dat\\mfcc_s5B.csv', 'MFCCs','-ascii');

wave_file = '..\\wav\\s8A.wav';
[speech, fs] = audioread( wave_file );
[MFCCs, FBE, FRAMES] = mfcc( speech , fs, Tw, Ts, alpha, @hamming, [LF HF], M, C+1, L);
save('..\\dat\\mfcc_s8A.csv', 'MFCCs','-ascii');

wave_file = '..\\wav\\s8B.wav';
[speech, fs] = audioread( wave_file );
[MFCCs, FBE, FRAMES] = mfcc( speech , fs, Tw, Ts, alpha, @hamming, [LF HF], M, C+1, L);
save('..\\dat\\mfcc_s8B.csv', 'MFCCs','-ascii');
