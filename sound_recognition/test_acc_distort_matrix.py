import numpy

# Assume it is a square matrix q * q
#   To loop by following sequence: 0-col, , 0-row,, 1-col, 1-row, 2-col, 2-row ...
#       q : size of a side of a matrix
#       m : index of the matrix; m-col & m-row
#       n : the index with starting point controlled by m
#       k : column and row selection

q = 13
for m in range(0, q):
    for n in range(m, q):
        for k in range(2):
            nearest_cells = numpy.empty(0)
            if k == 0:
                print("%d, %d" % (m, n))
                if m == 0 and n == 0:
                    print("0, 0")
                    #numpy.append(0)
                elif m == 0 and n != 0:
                    print("%d, %d" % (0, n-1))
                    #numpy.append()
                elif m != 0 and n != 0:
                    print("%d, %d" % (m, n-1))
                    print("%d, %d" % (m-1, n))
                    print("%d, %d" % (m-1, n-1))
                    #numpy.append()
                    #numpy.append()
                    #numpy.append()
            elif k == 1:
                print("%d, %d" % (n, m))
                if m == 0 and n == 0:
                    print("0, 0")
                    #numpy.append(0)
                elif m == 0 and n != 0:
                    print("%d, %d" % (n-1, 0))
                    #numpy.append()
                elif m != 0 and n != 0:
                    print("%d, %d" % (n-1, m))
                    print("%d, %d" % (n, m-1))
                    print("%d, %d" % (n-1, m-1))
                    #numpy.append()
                    #numpy.append()
                    #numpy.append()
            input("\n")
