"""
revision 0.1

Structure
---------
class MechBlock
    def init()
        cgx, cgy, cgz, Ixx, Iyy, Izz, Ixy, Ixz, Iyz, mass     #attributes
    def LoadPhysProp(str(PhysPropFileDir))
    def ValExtr(str(TextLine))
    def ConvertUnit(val, unit)

def RigidBodyModelCalc()

# class RigidBodyModel
#     def init(BlkA_Dir, BlkB_Dir, BlkC_dir, BlkD_Dir, Mod_Dir)
#         BlockA, BlockB, BlockC, BlockD, Module
#     def PhysParaInit()
#     def RigidBodyModelCalc()
"""


""" Import Third Party Library """
import numpy as np
from scipy import linalg
import fire



#def RigidBodyModelCalc(**para):
def RigidBodyModelCalc(
    x1, y1, z1, x2, y2, z2, x3, y3, z3, x4, y4, z4,
    k1x, k1y, k1z, k1rx, k1ry, k1rz,
    k2x, k2y, k2z, k2rx, k2ry, k2rz,
    k3x, k3y, k3z, k3rx, k3ry, k3rz,
    k4x, k4y, k4z, k4rx, k4ry, k4rz,
    Ixx, Ixy, Iyy, Iyz, Izz, Ixz,
    cgx, cgy, cgz, mass
    ):

    """
    A function to calculate the frequencies of rigid body model

    Args:
        x1, y1, z1 (Num):   Block 1 Center of Mass (Meter)
        x2, y2, z2 (Num):   Block 2 Center of Mass (Meter)
        x3, y3, z3 (Num):   Block 3 Center of Mass (Meter)
        x4, y4, z4 (Num):   Block 4 Center of Mass (Meter)
        k1x, k1y, k1z, k1rx, k1ry, k1rz (Num):   Block 1 Stiffness (N/m)(N*m/rad)
        k2x, k2y, k2z, k2rx, k2ry, k2rz (Num):   Block 2 Stiffness (N/m)(N*m/rad)
        k3x, k3y, k3z, k3rx, k3ry, k3rz (Num):   Block 3 Stiffness (N/m)(N*m/rad)
        k4x, k4y, k4z, k4rx, k4ry, k4rz (Num):   Block 4 Stiffness (N/m)(N*m/rad)
        Ixx, Ixy, Iyy, Iyz, Izz, Ixz (Num):   Moment of inertia in global system(kg*m^2)
        cgx, cgy, cgz (Num):   Center of Mass of Module (Meter)
        mass (Num):   Mass of Module (kg)

    Returns:
        list: 6 calculated frequencies in ascending order
    """


    """ Variable Initiation """
    # The code below is for RigidBodyModelCalc(**para)
    # for key, val in para.items():
    #     exec(key +  "=val")

    # Assign new values to the variables by subitile
    for num in range(1,5):
        for axis in ["x", "y", "z"]:
            cg_var = "cg" + axis
            axis_var = axis + str(num)
            exec(axis_var + " -= " + cg_var)

    """ Matrix Initiation """
    I_global = np.array([
        [Ixx, -Ixy, -Ixz],
        [-Ixy, Iyy, -Iyz],
        [-Ixz, -Iyz, Izz],
        ])

    cg_vector = np.array([
        [cgx],
        [cgy],
        [cgz],
        ])

    I_cg = I_global - mass * (np.transpose(cg_vector) * cg_vector * np.eye(3) - cg_vector * np.transpose(cg_vector))

    Ixx, Iyy, Izz = I_cg[0,0], I_cg[1,1], I_cg[2,2]
    Ixy, Ixz, Iyz = -I_cg[0,1], -I_cg[0,2], -I_cg[1,2]

    M = np.array([
        [mass, 0, 0, 0, 0, 0],
        [0, mass, 0, 0, 0, 0],
        [0, 0, mass, 0, 0, 0],
        [0, 0, 0, Ixx, -Ixy, -Ixz],
        [0, 0, 0, -Ixy, Iyy, -Iyz],
        [0, 0, 0, -Ixz, -Iyz, Izz],
        ])

    k11 = k1x + k2x + k3x + k4x
    k12 = 0
    k13 = 0
    k14 = 0
    k15 = k1x*z1 + k2x*z2 + k3x*z3 + k4x*z4
    k16 = -k1x*y1 - k2x*y2 - k3x*y3 - k4x*y4

    k21 = 0
    k22 = k1y + k2y + k3y + k4y
    k23 = 0
    k24 = -k1y*z1 - k2y*z2 - k3y*z3 - k4y*z4
    k25 = 0
    k26 = k1y*x1 + k2y*x2 + k3y*x3 + k4y*x4

    k31 = 0
    k32 = 0
    k33 = k1z + k2z + k3z + k4z
    k34 = k1z*y1 + k2z*y2 + k3z*y3 + k4z*y4
    k35 = -k1z*x1 - k2z*x2 - k3z*x3 - k4z*x4
    k36 = 0

    k41 = 0
    k42 = -k1y*z1 - k2y*z2 - k3y*z3 - k4y*z4
    k43 = k1z*y1 + k2z*y2 + k3z*y3 + k4z*y4
    k44 = k1y*z1*z1 + k2y*z2*z2 + k3y*z3*z3 + k4y*z4*z4 + k1z*y1*y1 + k2z*y2*y2 + k3z*y3*y3 + k4z*y4*y4
    k45 =-k1z*x1*y1 - k2z*x2*y2 - k3z*x3*y3 - k4z*x4*y4
    k46 = -k1y*x1*z1 - k2y*x2*z2 - k3y*x3*z3 - k4y*x4*z4

    k51 = k1x*z1 + k2x*z2 + k3x*z3 + k4x*z4
    k52 = 0
    k53 = -k1z*x1 - k2z*x2 - k3z*x3 - k4z*x4
    k54 = -k1z*x1*y1 - k2z*x2*y2 - k3z*x3*y3 - k4z*x4*y4
    k55 = k1x*z1*z1 + k1z*x1*x1 +k2x*z2*z2 + k2z*x2*x2 +  k3x*z3*z3 + k3z*x3*x3 +k4x*z4*z4 + k4z*x4*x4
    k56 = -k1x*y1*z1 - k2x*y2*z2 - k3x*y3*z3 -k4x*y4*z4

    k61 = -k1x*y1 - k2x*y2 - k3x*y3 - k4x*y4
    k62 = k1y*x1 + k2y*x2 + k3y*x3 + k4y*x4
    k63 = 0
    k64 = -k1y*x1*z1 - k2y*x2*z2 - k3y*x3*z3 - k4y*x4*z4
    k65 = -k1x*y1*z1 - k2x*y2*z2 - k3x*y3*z3 -k4x*y4*z4
    k66 = k1x*y1*y1 + k1y*x1*x1 + k2x*y2*y2 + k2y*x2*x2 + k3x*y3*y3 + k3y*x3*x3 + k4x*y4*y4 + k4y*x4*x4

    K = np.array([
        [k11, k12, k13, k14, k15, k16],
        [k21, k22, k23, k24, k25, k26],
        [k31, k32, k33, k34, k35, k36],
        [k41, k42, k43, k44, k45, k46],
        [k51, k52, k53, k54, k55, k56],
        [k61, k62, k63, k64, k65, k66],
        ])

    """ Find Eigen Value """
    V = linalg.eigvals(K, M)

    """ Transform to frequence """
    X = (np.sqrt(V))/(2*np.pi)

    """ Convert complex num to real num, then form an ascending list """
    X = sorted(np.real(X).tolist())
    return X



if __name__ == "__main__":
    fire.Fire()
    #BlockA = MechBlock("..\.gitignore\physprop.txt")
    #print BlockA.Ixx["val"]

    # # Testing Variables
    # VarDict = {
    #     "x1": -1, "y1": -1, "z1": 0,
    #     "x2": 1, "y2": -1, "z2": 0,
    #     "x3": -1, "y3": 1, "z3": 0,
    #     "x4": 1, "y4": 1, "z4": 0,
    #     "k1x": 12340000, "k1y": 1000000, "k1z": 1000000, "k1rx": 1, "k1ry": 2, "k1rz": 3,
    #     "k2x": 1000000, "k2y": 1000000, "k2z": 1000000, "k2rx": 4, "k2ry": 5, "k2rz": 6,
    #     "k3x": 1000000, "k3y": 1000000, "k3z": 1000000, "k3rx": 7, "k3ry": 8, "k3rz": 9,
    #     "k4x": 1000000, "k4y": 1000000, "k4z": 1000000, "k4rx": 10, "k4ry": 11, "k4rz": 12,
    #     "Ixx": 1, "Ixy": 0,
    #     "Iyy": 1, "Iyz": 0,
    #     "Izz": 1, "Ixz": 0,
    #     "cgx": 0, "cgy": 0, "cgz": 0, "mass": 201.3438,
    #     }
    #
    # # Testing Function Call
    # X = RigidBodyModelCalc(**VarDict)
    #
    # raw_input(X)
