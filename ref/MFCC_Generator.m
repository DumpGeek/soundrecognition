addpath('.//mfcc');     % Add MFCC Library Path


function MFCCs = MFCC_Generator(AudioFileName)
    % Define variables
    Tw = 25;                % analysis frame duration (ms)
    Ts = 10;                % analysis frame shift (ms)
    alpha = 0.97;           % preemphasis coefficient
    M = 20;                 % number of filterbank channels 
    C = 12;                 % number of cepstral coefficients
    L = 22;                 % cepstral sine lifter parameter
    LF = 300;               % lower frequency limit (Hz)
    HF = 3700;              % upper frequency limit (Hz)
    
    wav_file = sprintf('./Audio//%s.wav', AudioFileName);% input audio filename

    % Read speech samples, sampling rate and precision from file
    [ speech, fs, nbits ] = wavread(wav_file);
    %[ speech, fs, nbits ] = wavread( wav_file );


    % Feature extraction (feature vectors as columns)
    [ MFCCs, FBEs, frames ] = ...
                    mfcc( speech, fs, Tw, Ts, alpha, @hamming, [LF HF], M, C+1, L );


    % Generate data needed for plotting 
    [ Nw, NF ] = size( frames );                % frame length and number of frames
    time_frames = [0:NF-1]*Ts*0.001+0.5*Nw/fs;  % time vector (s) for frames 
    time = [ 0:length(speech)-1 ]/fs;           % time vector (s) for signal samples 
    logFBEs = 20*log10( FBEs );                 % compute log FBEs for plotting
    logFBEs_floor = max(logFBEs(:))-50;         % get logFBE floor 50 dB below max
    logFBEs( logFBEs<logFBEs_floor ) = logFBEs_floor; % limit logFBE dynamic range


    % Generate plots
    figure('Position', [30 30 800 600], 'PaperPositionMode', 'auto', ... 
              'color', 'w', 'PaperOrientation', 'landscape', 'Visible', 'on' ); 

    subplot( 311 );
    plot( time, speech, 'k' );
    xlim( [ min(time_frames) max(time_frames) ] );
    xlabel( 'Time (s)' ); 
    ylabel( 'Amplitude' ); 
    title( 'Speech waveform'); 

    subplot( 312 );
    imagesc( time_frames, [1:M], logFBEs ); 
    axis( 'xy' );
    xlim( [ min(time_frames) max(time_frames) ] );
    xlabel( 'Time (s)' ); 
    ylabel( 'Channel index' ); 
    title( 'Log (mel) filterbank energies'); 

    subplot( 313 );
    imagesc( time_frames, [1:C], MFCCs(2:end,:) ); % HTK's TARGETKIND: MFCC
    %imagesc( time_frames, [1:C+1], MFCCs );       % HTK's TARGETKIND: MFCC_0
    axis( 'xy' );
    xlim( [ min(time_frames) max(time_frames) ] );
    xlabel( 'Time (s)' ); 
    ylabel( 'Cepstrum index' );
    title( 'Mel frequency cepstrum' );

    % Set color map to grayscale
    colormap( 1-colormap('gray') ); 

    % Print figure to png files
    save(sprintf('MFCC_%s.dat', AudioFileName), 'MFCCs','-ascii');
    print('-dpng', sprintf('MFCC_%s.png', AudioFileName));
    close all;
end%Function End


function  [ speech, fs, nbits ] = GetTrimDataFromFile(AudioFileName , TargetLengthms)%Load a audio file and return a 500ms trimmed data
 
  AudioFileName
  [ speech, fs, nbits ] = wavread( AudioFileName );
  
  %Init Variables
  DARawAudio = speech;
  ArraySize = size(speech,1)   %Get audio stream length(total num of samples)
  Fs = 44100;                       %Sampling frequency     
  FrameSize = 1102                  %Frame Size = (25ms / 1000ms)*44100 = 1102 Samples
  NOverSize = 441                   %Non-Overlapping Size = (10ms / 1000ms)*44100 = 441 Samples
  TotalFrame = (int32)((ArraySize - (FrameSize-NOverSize))/NOverSize + 1)%Total Frame num in this signal
  T1 = -1;                          %Start Point Frame
  
  %Find Zero Crossing Rate amd Engery in each frame
  FrameZeroCross = zeros(TotalFrame);
  FrameEngery = zeros(TotalFrame);
  for i = 1:TotalFrame-1
    ZeroCrossCnt = 0;
    AvgEngery = 0.0;
    for k = 0:FrameSize-3 %Loop every samples in a frame
      if (i*NOverSize+k+2) < ArraySize && sign(DARawAudio(i*NOverSize+k+1)) != sign(DARawAudio(i*NOverSize+k+2))%Zero Crossing Point Found
        ZeroCrossCnt++;
      endif
      if (i*NOverSize+k+1) < ArraySize
        AvgEngery += DARawAudio(i*NOverSize+k+1)*DARawAudio(i*NOverSize+k+1);
      endif
    end
    AvgEngery = AvgEngery / (FrameSize-1);
    FrameZeroCross(i) = ZeroCrossCnt; %Save Zero Crossing Rate in this frame
    FrameEngery(i) = AvgEngery;       %Save Engery in this frame
  end
  %----------------------------------------

  %Find Start Points
  FoundT1 = false;
  Thersold1 = 40.0;    % Min Zero Crossing Rate
  Thersold2 = 0.05;    % Min Engery Level
  while !FoundT1
    Thersold1 = Thersold1 * 0.9 % Step Down Thersold if cannot find T!
    Thersold2 = Thersold2 * 0.9 % Step Down Thersold if cannot find T!
    for i = 0:TotalFrame / 2
      %Start Point Detection
      if !FoundT1
        HighLevel = false;
        for k = 1:3%For 3 successive samples
          if (FrameZeroCross(i+k) > Thersold1 && FrameEngery(i+k) > Thersold2)
            HighLevel = true;
          endif
          if HighLevel
            T1 = i
            FoundT1 = true;
            break;
           endif
         end
       endif
    end
  end
  
  TargetLengthSample = fs * TargetLengthms / 1000
  if (T1 + TargetLengthSample) < ArraySize
    speech = DARawAudio(T1:T1+TargetLengthSample);
  else if (ArraySize > TargetLengthSample) 
    speech = DARawAudio(ArraySize - TargetLengthSample:ArraySize);
    endif
  endif
  m = size(speech,1)
end%Function End


%
%Main Loop

for i = 0:9
  File = sprintf('s%dA', i);%Set File Name
  MFCC_Generator(File);               %Run MFCC
  File = sprintf('s%dB', i);%Set File Name
  MFCC_Generator(File);               %Run MFCC
end
%

% EOF
